<?php

// require MySQL Connection
require ('database/DBController.php');

// require Product Class
require ('database/ProductController.php');

// DBController object
$db = new DBController("localhost", "root", "", "productdb");

// Product object
$productController = new ProductController($db);
$product_shuffle = $productController->getProducts();

if (isset($_POST['add-product'])) {
    $productController->addProduct();
}

if (isset($_POST['delete-product'])) {
    $productController->deleteProduct();
    echo "<meta http-equiv='refresh' content='0'>";
}
