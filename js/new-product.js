// Hide all special attribute inputs
$(document).ready(function() {
  $(".special-att").hide();
  $("#typeSwitcher").val("");
});

// Show special att input of product
$('#typeSwitcher').on('change', function() {
    $(".special-att").hide();
    $("." + this.value).show();
    $("." + this.value + " input").prop('required',true);
});

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  'use strict'

  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation')

    // Loop over them and prevent submission
    Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault()
          event.stopPropagation()
        }
        form.classList.add('was-validated')
      }, false)
    })
  }, false)
}())

// Check input fields for numeric value
function onlyNumberKey(evt) { 
  var charCode = (evt.which) ? evt.which : evt.keyCode;
  if (charCode != 46 && charCode > 31 
    && (charCode < 48 || charCode > 57))
     return false;

  return true;
} 