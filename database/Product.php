<?php

class Product
{
    private $sku, $name, $price, $type;

    public function __construct($sku, $name, $price, $type)
    {
        $this->sku = $sku;
        $this->name = $name;        
        $this->price = $price;
        $this->type = $type;
    }

    public function getSku() {
        return $this->sku;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getPrice() {
        return $this->price;
    }
    
    public function getType() {
        return $this->type;
    }

    public function getSqlInsert() {
        return "INSERT INTO producttb (product_sku, product_name, product_price, product_type, product_att) VALUES
        ('$this->sku', '$this->name', $this->price, '$this->type', ";
    }

}