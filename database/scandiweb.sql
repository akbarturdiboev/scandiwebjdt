-- 
-- Create db
-- 
CREATE DATABASE IF NOT EXISTS productdb;

-- 
-- Create product table
-- 
CREATE TABLE IF NOT EXISTS producttb (
    product_sku 	varchar(10) NOT NULL PRIMARY KEY,
    product_name  	varchar(100) NOT NULL,
    product_price 	decimal(5, 2) NOT NULL,
    product_type  	varchar(15) NOT NULL,
    product_att   	varchar(50) NOT NULL
);


-- 
-- Insert values into product table 
-- 
INSERT INTO producttb (product_sku, product_name, product_price, product_type, product_att) VALUES
('JVC200123', 'Acme DISC', 1.00, 'dvd-disc', '700'),
('GGWP0007', 'War and Peace', 20.00, 'book', '2'),
('TR120555', 'Chair', 40.00, 'furniture', '24x45x15');