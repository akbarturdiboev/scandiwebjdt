<?php

class Book extends Product
{
    private $weight;

    public function __construct($sku, $name, $price, $type, $weight)
    {
        parent::__construct($sku, $name, $price, $type);
        $this->weight = $weight;
    }

    public function getWeight() {
        return $this->weight;
    }

    public function getSqlInsert() {
        return parent::getSqlInsert() . "'$this->weight')"; 
    }
}