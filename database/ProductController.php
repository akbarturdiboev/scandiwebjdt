<?php

require ('Product.php');
require ('DvdDisc.php');
require ('Book.php');
require ('Furniture.php');

// Use to fetch product data
class ProductController
{
    public $db = null;

    public function __construct(DBController $db)
    {
        if (!isset($db->con)) return null;
        $this->db = $db;
    }

    // fetch product data using getData Method
    public function getProducts($table = 'producttb'){
        return $this->db->con->query("SELECT * FROM {$table}");
    }
    
    // insert product into the table
    public function addProduct() {
        $sku = isset($_POST['sku']) ? $_POST['sku'] : '';
        $name = isset($_POST['name']) ? $_POST['name'] : '';
        $price = isset($_POST['price']) ? $_POST['price'] : '';
        $type = isset($_POST['type']) ? $_POST['type'] : '';
        $size = isset($_POST['disk-size']) ? $_POST['disk-size'] : '';
        $weight = isset($_POST['book-weight']) ? $_POST['book-weight'] : '';
        $height = isset($_POST['furniture-height']) ? $_POST['furniture-height'] : '';
        $width = isset($_POST['furniture-width']) ? $_POST['furniture-width'] : '';
        $length = isset($_POST['furniture-length']) ? $_POST['furniture-length'] : '';

        $product = null;

        if($type == "dvd-disc") {
            $product = new DvdDisc($sku, $name, $price, $type, $size);

        } elseif($type == "book") {
            $product = new Book($sku, $name, $price, $type, $weight);

        } else {
            $product = new Furniture($sku, $name, $price, $type, $height, $width, $length);
        }

        mysqli_query($this->db->getConnection(), $product->getSqlInsert());
    }


    // delete products from the table
    public function deleteProduct() {
        $actionType = isset($_POST['action-type']) ? $_POST['action-type'] : '';

        if($actionType == "selectedOnly") {
            if(isset($_POST['sku'])) {
                foreach($_POST['sku'] as $sku) {
                    $sql = "DELETE FROM producttb WHERE product_sku = '$sku'";
                    mysqli_query($this->db->getConnection(), $sql); 
                }
            }
        
        } else {
            $sql = "DELETE FROM producttb";
            mysqli_query($this->db->getConnection(), $sql); 
        } 
    }

}