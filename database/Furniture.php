<?php

class Furniture extends Product
{
    private $specialAtt, $height, $width, $length;

    public function __construct($sku, $name, $price, $type, $height, $width, $length)
    {
        parent::__construct($sku, $name, $price, $type);
        $this->height = $height;
        $this->width = $width;
        $this->length = $length;
        $this->specialAtt = $this->getSpecialAtt();
    }

    public function getSpecialAtt() {
        $this->specialAtt = $this->height . "x" . $this->width . "x" . $this->length;
        return $this->specialAtt;
    }

    public function getHeight() {
        return $this->height;
    }

    public function getWidth() {
        return $this->width;
    }

    public function getLength() {
        return $this->length;
    }

    public function getSqlInsert() {
        return parent::getSqlInsert() . "'$this->specialAtt')"; 
    }
}