<?php

class DvdDisc extends Product
{
    private $size;

    public function __construct($sku, $name, $price, $type, $size)
    {
        parent::__construct($sku, $name, $price, $type);
        $this->size = $size;
    }

    public function getSize() {
        return $this->size;
    }

    public function getSqlInsert() {
        return parent::getSqlInsert() . "'$this->size')"; 
    }
}