<?php

function component($sku, $name, $price, $type, $productatt) {
    $measurement = "";
    $unit = "";

    if($type == "dvd-disc") {
        $measurement = "Size";
        $unit = "MB";
    } elseif($type == "book") {
        $measurement = "Weight";
        $unit = "KG";
    } else {
        $measurement = "Dimensions";
        $unit = "";
    }

    $element = "
        <div class='column'>
            <div class='grid-product'>
                <input type='checkbox' value='$sku' name='sku[]'>
                <div class='product-details'>
                    <p>$sku</p>
                    <p>$name</p>
                    <p>$price $</p>
                    <p>$measurement: $productatt $unit</p>
                </div>
            </div>    
        </div>
    ";
    
    echo $element;
}