    <!-- start #product list form -->
    <form id="product-list-form" action="product-list.php" method="post">
        <div class="container">
            <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-0 mb-3 bg-white border-bottom">
                <h3 class="my-0 mr-md-auto font-weight-normal">Product List</h5>
                <nav class="my-2 my-md-0 mr-md-3">
                    <select name="action-type" class="custom-select d-block w-100">
                        <option value="selectedOnly">Delete Selected Products</option>
                        <option value="massDelete">Mass Delete Action</option>
                    </select>
                </nav>
                <button class="btn btn-outline-secondary" type="submit" name="delete-product">Apply</button>
            </div>


            <div class="row">
                <?php
                    require_once('./template/_component.php');
                    if(!empty($product_shuffle)) {
                        while($row=mysqli_fetch_assoc($product_shuffle)) {
                            component($row['product_sku'], $row['product_name'], $row['product_price'], $row['product_type'], $row['product_att']);
                        }  
                    }
                ?>
            </div>
        </div>
    </form>
    <!-- !start #product list form -->
