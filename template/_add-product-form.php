  
  <!-- start add product form -->
   <form class="needs-validation" id="new-product-form" action="new-product.php" method="post" autocomplete="off" novalidate>
    <div class="container">
      <div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-0 mb-3 bg-white border-bottom">
        <h3 class="my-0 mr-md-auto font-weight-normal">Product List</h3>
        <button class="btn btn-outline-secondary" type="submit" name="add-product">Save</button>
      </div>

      <div class="form-group row py-2">
        <label for="sku" class="col-sm-2 col-form-label">SKU</label>
        <div class="col-sm-3">
          <input type="text" class="form-control" name="sku" maxlength="10" placeholder="JVC200123" required>
          <div class="invalid-feedback">
            Product SKU is required.
          </div>
        </div>

      </div>

      <div class="form-group row py-2">
        <label for="name" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-3">
          <input type="text" class="form-control" name="name" maxlength="100" placeholder="Acme DISC" required>
          <div class="invalid-feedback">
            Product name is required.
          </div>
        </div>
      </div>

      <div class="form-group row py-2">
        <label for="price" class="col-sm-2 col-form-label">Price</label>
        <div class="col-sm-3">
          <input type="float" class="form-control" name="price" maxlength="7" placeholder="15.00" onkeypress="return onlyNumberKey(event)" required>
          <div class="invalid-feedback">
            Product price is required.
          </div>
        </div>
      </div>

      <div class="form-group row py-2">
        <label for="type" class="col-sm-2 col-form-label">Type Switcher</label>
        <div class="col-sm-3">
          <select id="typeSwitcher" name="type" class="custom-select d-block w-100" required>
            <option selected disabled value="">Choose...</option>
            <option value="dvd-disc">DVD-<i>disc</i></option>
            <option value="book"><i>Book</i></option>
            <option value="furniture"><i>Furniture</i></option>
          </select>
          <div class="invalid-feedback">
            Please select the type of the product.
          </div>
        </div>
      </div>


      <div class="dvd-disc special-att">
        <div class="form-group row py-2">
          <label for="size" class="col-sm-2 col-form-label">Size</label>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="disk-size" onkeypress="return onlyNumberKey(event)" placeholder="700">
            <small class="text-muted">Please provide size in MB</small>
            <div class="invalid-feedback">
              Product size is required.
            </div>
          </div>
        </div>
      </div>


      <div class="book special-att">
        <div class="form-group row py-2">
          <label for="book-weight" class="col-sm-2 col-form-label">Weight</label>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="book-weight" onkeypress="return onlyNumberKey(event)" placeholder="2">
            <small class="text-muted">Please provide weight in KG</small>
            <div class="invalid-feedback">
              Product weight is required.
            </div>
          </div>
        </div>
      </div>

      <div class="furniture special-att">
        <div class="form-group row py-2">
          <label for="furniture-height" class="col-sm-2 col-form-label">Height</label>
          <div class="col-sm-3">
            <input type="text" class="form-control" name="furniture-height" onkeypress="return onlyNumberKey(event)" placeholder="24">
            <div class="invalid-feedback">
              Product height is required.
            </div>
          </div>
        </div>

        <div class="form-group row py-2">
          <label for="furniture-width" class="col-sm-2 col-form-label">Width</label>
          <div class="col-sm-3">
            <input type="float" class="form-control" name="furniture-width" onkeypress="return onlyNumberKey(event)" placeholder="45">
            <div class="invalid-feedback">
              Product width is required.
            </div>
          </div>
        </div>

        <div class="form-group row py-2">
          <label for="furniture-length" class="col-sm-2 col-form-label">Length</label>
          <div class="col-sm-3">
            <input type="float" class="form-control" name="furniture-length" onkeypress="return onlyNumberKey(event)" placeholder="15">
            <small class="text-muted">Please provide dimensions in HxWxL format</small>
            <div class="invalid-feedback">
              Product length is required.
            </div>
          </div>
        </div>
      </div>
  </form>
  <!-- !start add product form -->
